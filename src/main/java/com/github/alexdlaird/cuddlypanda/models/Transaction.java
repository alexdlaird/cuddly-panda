package com.github.alexdlaird.cuddlypanda.models;

public class Transaction {
    private long id;

    private User from;

    private User to;

    private double amount;

    public Transaction(long id, User from, User to, double amount) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public User getFrom() {
        return from;
    }

    public User getTo() {
        return to;
    }

    public double getAmount() {
        return amount;
    }
}
