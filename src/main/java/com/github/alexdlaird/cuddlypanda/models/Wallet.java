package com.github.alexdlaird.cuddlypanda.models;

public class Wallet {
    private long id;

    private double balance;

    private User user;

    public Wallet(long id, double initialBalance, User user) {
        this.id = id;
        this.balance = initialBalance;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    /**
     * Returns the current balance of the wallet.
     */
    public double getBalance() {
        return balance;
    }

    public User getUser() {
        return user;
    }

    /**
     * Adds the given amount to the wallet and returns the new balance.
     */
    public double addFunds(double amount) {
        this.balance += amount;

        return this.balance;
    }

    /**
     * Subtracts the given amount from the wallet and returns the new balance.
     */
    public double deductFunds(double amount) {
        this.balance -= amount;

        return this.balance;
    }
}
