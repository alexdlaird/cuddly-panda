package com.github.alexdlaird.cuddlypanda.models;

public class PaymentRequest {
    private long id;

    private User from;

    private User to;

    private double amount;

    private boolean approved;

    public PaymentRequest(long id, User from, User to, double amount) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.approved = false;
    }

    public long getId() {
        return id;
    }

    public User getFrom() {
        return from;
    }

    public User getTo() {
        return to;
    }

    public double getAmount() {
        return amount;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
