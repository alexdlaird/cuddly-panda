package com.github.alexdlaird.cuddlypanda.services;

import com.github.alexdlaird.cuddlypanda.models.PaymentRequest;

interface PaymentRequestClient {
    /**
     * Make a PaymentRequest from one user to another, which will pend until the other user approves the PaymentRequest.
     *
     * @param fromUserId The user being requested money of (who will be paying).
     * @param toUserId   The user making the payment request (which will be paid).
     * @param amount     The amount to be requested.
     */
    PaymentRequest makeRequest(long fromUserId, long toUserId, double amount);

    /**
     * Retrieve a list of currently pending PaymentRequests to be paid by the given user.
     */
    Iterable<PaymentRequest> getRequestsForUser(long userId);
}
