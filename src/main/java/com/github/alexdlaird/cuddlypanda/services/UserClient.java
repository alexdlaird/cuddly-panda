package com.github.alexdlaird.cuddlypanda.services;

import com.github.alexdlaird.cuddlypanda.models.User;

public interface UserClient {
    /**
     * Create a new User and Wallet with the given initial balance.
     */
    User createUser(String username, double initialBalance);

    /**
     * Retrieve the balance for the given user.
     */
    double getBalanceForUser(long userId);
}
