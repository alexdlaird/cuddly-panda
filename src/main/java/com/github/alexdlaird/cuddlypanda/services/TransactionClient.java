package com.github.alexdlaird.cuddlypanda.services;

import com.github.alexdlaird.cuddlypanda.models.Transaction;

interface TransactionClient {
    /**
     * Approve the given PaymentRequest, marking it as "approved".
     *
     * @return The processed Transaction.
     */
    Transaction makePayment(long paymentRequestId);

    /**
     * Make an authorized Transaction from one user to another without a PaymentRequest. This command is only executable
     * by the payee, as they are the ones approving the withdrawal from their own account.
     *
     * @return The processed Transaction.
     */
    Transaction makePayment(long fromUserId, long toUserId, double amount);
}
