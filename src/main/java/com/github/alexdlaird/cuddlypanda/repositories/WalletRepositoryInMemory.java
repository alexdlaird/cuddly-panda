package com.github.alexdlaird.cuddlypanda.repositories;

import com.github.alexdlaird.cuddlypanda.models.User;
import com.github.alexdlaird.cuddlypanda.models.Wallet;

import java.util.HashMap;
import java.util.Map;

public class WalletRepositoryInMemory implements WalletRepository {
    private Map<Long, Wallet> cache = new HashMap<>();

    private Map<Long, Wallet> cacheByUser = new HashMap<>();

    private long pkPointer = 0;

    @Override
    public Wallet create(double initialBalance, User user) {
        Wallet wallet = new Wallet(this.pkPointer, initialBalance, user);
        this.pkPointer += 1;

        this.save(wallet);

        return wallet;
    }

    @Override
    public Wallet getById(long id) {
        return this.cache.get(id);
    }

    @Override
    public Wallet getByUser(long userId) {
        return this.cache.get(userId);
    }

    private void save(Wallet wallet) {
        this.cache.put(wallet.getId(), wallet);
        this.cacheByUser.put(wallet.getUser().getId(), wallet);
    }
}
