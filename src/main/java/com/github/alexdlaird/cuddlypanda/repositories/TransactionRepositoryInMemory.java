package com.github.alexdlaird.cuddlypanda.repositories;

import com.github.alexdlaird.cuddlypanda.models.Transaction;
import com.github.alexdlaird.cuddlypanda.models.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionRepositoryInMemory implements TransactionRepository {
    private Map<Long, Transaction> cache = new HashMap<>();

    private Map<Long, List<Transaction>> cacheByUser = new HashMap<>();

    private long pkPointer = 0;

    @Override
    public Transaction create(User from, User to, double amount) {
        Transaction transaction = new Transaction(this.pkPointer, from, to, amount);
        this.pkPointer += 1;

        this.save(transaction);

        return transaction;
    }

    @Override
    public Transaction getById(long id) {
        return this.cache.get(id);
    }

    @Override
    public Iterable<Transaction> getByUser(long userId) {
        return this.cacheByUser.get(userId);
    }

    private void save(Transaction transaction) {
        this.cache.put(transaction.getId(), transaction);

        long fromUserId = transaction.getFrom().getId();
        if (!this.cacheByUser.containsKey(fromUserId)) {
            this.cacheByUser.put(fromUserId, new ArrayList<>());
        }
        this.cacheByUser.get(fromUserId).add(transaction);

        long toUserId = transaction.getFrom().getId();
        if (!this.cacheByUser.containsKey(toUserId)) {
            this.cacheByUser.put(toUserId, new ArrayList<>());
        }
        this.cacheByUser.get(toUserId).add(transaction);
    }
}
