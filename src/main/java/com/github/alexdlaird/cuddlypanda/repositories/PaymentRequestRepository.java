package com.github.alexdlaird.cuddlypanda.repositories;

import com.github.alexdlaird.cuddlypanda.models.PaymentRequest;
import com.github.alexdlaird.cuddlypanda.models.User;

interface PaymentRequestRepository {
    PaymentRequest create(User from, User to, double amount);

    PaymentRequest getById(long id);

    Iterable<PaymentRequest> getPendingRequestsForUser(long userId);
}
