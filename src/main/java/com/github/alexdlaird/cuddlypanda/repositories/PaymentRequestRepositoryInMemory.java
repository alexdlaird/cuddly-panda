package com.github.alexdlaird.cuddlypanda.repositories;

import com.github.alexdlaird.cuddlypanda.models.PaymentRequest;
import com.github.alexdlaird.cuddlypanda.models.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentRequestRepositoryInMemory implements PaymentRequestRepository {
    private Map<Long, PaymentRequest> cache = new HashMap<>();

    private Map<Long, List<PaymentRequest>> cachePendingForUser = new HashMap<>();

    private long pkPointer = 0;

    @Override
    public PaymentRequest create(User from, User to, double amount) {
        PaymentRequest paymentRequest = new PaymentRequest(this.pkPointer, from, to, amount);
        this.pkPointer += 1;

        this.save(paymentRequest);

        return paymentRequest;
    }

    @Override
    public PaymentRequest getById(long id) {
        return this.cache.get(id);
    }

    @Override
    public Iterable<PaymentRequest> getPendingRequestsForUser(long userId) {
        return null;
    }

    private void save(PaymentRequest paymentRequest) {
        this.cache.put(paymentRequest.getId(), paymentRequest);

        long fromUserId = paymentRequest.getFrom().getId();
        if (!this.cachePendingForUser.containsKey(fromUserId)) {
            this.cachePendingForUser.put(fromUserId, new ArrayList<>());
        }
        this.cachePendingForUser.get(fromUserId).add(paymentRequest);
    }
}
