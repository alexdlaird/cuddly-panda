package com.github.alexdlaird.cuddlypanda.repositories;

import com.github.alexdlaird.cuddlypanda.models.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepositoryInMemory implements UserRepository {
    private Map<Long, User> cache = new HashMap<>();

    private long pkPointer = 0;

    @Override
    public User create(String username) {
        User user = new User(this.pkPointer, username);
        this.pkPointer += 1;

        this.save(user);

        return user;
    }

    @Override
    public User getById(long id) {
        return this.cache.get(id);
    }

    private void save(User user) {
        this.cache.put(user.getId(), user);
    }
}
