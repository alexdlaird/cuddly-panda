package com.github.alexdlaird.cuddlypanda.repositories;

import com.github.alexdlaird.cuddlypanda.models.Transaction;
import com.github.alexdlaird.cuddlypanda.models.User;

interface TransactionRepository {
    Transaction create(User from, User to, double amount);

    Transaction getById(long id);

    Iterable<Transaction> getByUser(long userId);
}
