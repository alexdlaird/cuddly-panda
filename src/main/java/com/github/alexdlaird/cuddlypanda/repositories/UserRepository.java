package com.github.alexdlaird.cuddlypanda.repositories;

import com.github.alexdlaird.cuddlypanda.models.User;

interface UserRepository {
    User create(String username);

    User getById(long id);
}
