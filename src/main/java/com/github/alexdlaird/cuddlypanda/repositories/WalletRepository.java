package com.github.alexdlaird.cuddlypanda.repositories;

import com.github.alexdlaird.cuddlypanda.models.User;
import com.github.alexdlaird.cuddlypanda.models.Wallet;

interface WalletRepository {
    Wallet create(double initialBalance, User user);

    Wallet getById(long id);

    Wallet getByUser(long userId);
}
